# The Sorty and Proverbs of Ahiqar the Wise

This GitLab project contains the TEI data and all relevant code
for processing the files and interact with their representatives
at TextGrid.

## Adding new data

Files from the directories `data` and `schema` are uploaded/synced
to TextGrid. You can alter the metadata files `.meta` to update
the metadata. If you add a new file, just add both: the data
file and the corresponding metadata file, but leave the URI
field empty. A URI is assigned automatically.

You will likely have to add new folders as well. In this case
create the folder together with corresponding metadata file.
