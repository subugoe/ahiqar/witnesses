<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
    version="3">
    <xsl:output method="html" indent="yes"/>
    <xsl:param name="date"/>
    <xsl:param name="dateTime"/>
    <xsl:param name="commitSha"/>
    <xsl:param name="pipelineUrl"/>


    <xsl:template match="/">
        <html lang="en">
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta http-equiv="X-UA-Compatible" content="ie=edge"/>

                <title>Validation Report for GFL Project</title>
                <!-- 

Timeless Template 

http://www.templatemo.com/tm-517-timeless

-->
                <!-- load CSS -->
                <link rel="stylesheet"
                    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400"/>
                <!-- Google web font "Open Sans" -->
                <link rel="stylesheet" href="css/bootstrap.min.css"/>
                <!-- https://getbootstrap.com/ -->
                <link rel="stylesheet" href="css/templatemo-style.css"/>
                <!-- Templatemo style -->
            </head>

            <body>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <header class="text-center tm-site-header">
                                <div class="tm-site-logo"/>
                                <h1 class="pl-4 tm-site-title">Validation Report</h1>
                                <hr/>
                                <p>This is the validation report from <xsl:value-of select="$date"
                                    />. You can review the <a href="{$pipelineUrl}">CI pipeline</a>
                                    that created this job.</p>
                                <p>For technical purposes: A JSON Array with all filenames of invalid files is served <a href="list.json">here</a>.</p>
                            </header>
                        </div>
                    </div>
                    <xsl:apply-templates/>
                    <hr/>
                    <!-- Footer -->
                    <footer class="row mt-5 mb-5">
                        <div class="col-lg-12">
                            <p class="text-center tm-text-gray tm-copyright-text mb-0">Copyright
                                &#169; <span class="tm-current-year">2021</span> SUB Göttingen |
                                Design: <a href="http://templatemo.com/tm-517-timeless"
                                    class="tm-text-white">Timeless</a>
                            </p>
                            <p class="text-center tm-text-gray tm-copyright-text mb-0">
                                This is the version from <xsl:value-of select="$dateTime"/> created on commit <xsl:value-of select="$commitSha"/>.
                            </p>
                        </div>
                    </footer>
                </div>
                <!-- .container -->


            </body>
        </html>

    </xsl:template>

    <xsl:template match="div[@class eq 'wellformedness']">
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="tm-welcome-text">Well-Formedness</h2>
                </div>
                <p>This is the XMLStarlet report mentioning all files that are invalid in terms of
                    XML.</p>
            </div>
            <div class="row tm-section-mb">
                <div class="col-lg-12">
                    <xsl:apply-templates/>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="div[@class eq 'jing']">
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="tm-welcome-text">JING</h2>
                </div>
                <p>This is the JING report presenting all warnings and errors based on RelaxNG
                    schema validation.</p>
            </div>
            <div class="row tm-section-mb">
                <div class="col-lg-12">
                    <xsl:apply-templates/>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="div[@class eq 'isoschematron']">
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="tm-welcome-text">ISO-Schematron</h2>
                </div>
                <p>This is the ISO-Schematron report listing all files that infringe with the business rules.</p>
            </div>
            <div class="row tm-section-mb">
                <div class="col-lg-12">
                    <xsl:apply-templates>
                        <xsl:sort select="normalize-space(./div[@class='filename'])"/>
                    </xsl:apply-templates>
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="div[@class eq 'isoschematron-log']">
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="tm-welcome-text">ISO-Schematron log</h2>
                </div>
                <p>In addition to the tests above, the ISO-Schematron log contains parsing errors regarding the previous stages. Entries here migh interrupted validation process.</p>
            </div>
            <div class="row tm-section-mb">
                <div class="col-lg-12">
                    <xsl:apply-templates>
                        <xsl:sort select="normalize-space(./div[@class='filename'])"/>
                    </xsl:apply-templates>
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template name="item" match="div[@class eq 'item']">
        <div class="tm-timeline-item">
            <div class="tm-timeline-item-inner">
                <img src="img/img0{count( preceding::div[@class eq 'item'] ) mod 5}.png" alt="Image"
                    class="rounded-circle tm-img-timeline"/>
                <div class="tm-timeline-connector">
                    <p class="mb-0">&#160;</p>
                </div>
                <div class="tm-timeline-description-wrap">
                    <div class="tm-bg-dark tm-timeline-description">
                        <xsl:apply-templates/>
                    </div>
                </div>
            </div>
            <xsl:variable name="thisFilename" select="normalize-space(./div[@class='filename'])" />
            <xsl:variable name="allFilenames" select="sort(parent::div//div[@class='filename']/normalize-space(.))" />
            <xsl:if test="$thisFilename ne $allFilenames[last()]">
                <div class="tm-timeline-connector-vertical"/>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="div[@class = 'filename']">
        <h3 class="tm-text-green tm-font-400">
            <a class="tm-text-green tm-font-400" href="https://gitlab.gwdg.de/subugoe/ahiqar/witnesses/-/blob/master/{ if (starts-with(normalize-space(.), 'data/')) then normalize-space(.) else ('data/' || normalize-space(.))}{if(./parent::div//div[@class='line']) then concat('#L', normalize-space(./parent::div//div[@class='line'])) else ()}"><xsl:value-of select="normalize-space(.)"/></a>
        </h3>
    </xsl:template>

    <xsl:template match="div[@class = 'description'][not(./ancestor::div[@class='isoschematron'])]">
        <p>
            <xsl:value-of select="normalize-space(.)"/>
        </p>
    </xsl:template>

    <xsl:template match="div[@class = 'description'][./ancestor::div[@class='isoschematron']]" priority="100">
        <ol class="schematron-failed">
            <xsl:if test="not(.//svrl:*)">
                <p class="parse-error">
                    Unable to parse ISO-Schematron report. (Most likely reason: Saxon interrupt on call for external entity. Please check referenced GND or bibliography items; Oxygen will guide you.)
                </p>
            </xsl:if>
            <xsl:apply-templates/>
        </ol>
    </xsl:template>

    <xsl:template match="svrl:failed-assert | svrl:failed-report">
        <li title="{string(./@test)}"><xsl:value-of
                select="normalize-space(./svrl:text)"/>
            <span class="location"><xsl:value-of
                select="replace(./@location, '\*:|\[namespace-uri.*?\]', '')"/></span>   
        </li>
    </xsl:template>

    <xsl:template match="div[@class = 'url'][contains(., 'http')]">
        <a class="tm-text-green float-right mb-0" href="{normalize-space(.)}"><xsl:value-of
                select="normalize-space(.)"/></a>
    </xsl:template>

    <xsl:template match="div[@class = 'line']">
        <p class="tm-text-green float-right mb-0">Error at line <xsl:value-of
                select="normalize-space(.)"/>, column <xsl:value-of
                select="following-sibling::div[@class = 'column']/normalize-space(.)"/>.</p>
    </xsl:template>

    <xsl:template match="div[@class = 'column']"/>
</xsl:stylesheet>
