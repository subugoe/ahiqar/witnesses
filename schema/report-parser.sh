#!/bin/bash


echo '<div class="report">' > ../report/report.html


echo '<div class="wellformedness">' >> ../report/report.html

while read -r line;
do
  echo "<div class=\"item\">"
  
  echo -n '<div class="filename">'
  echo -n "${line}" | sed 's data/  g'
  echo "</div>" # filename
  
  echo '<div class="description">'
  echo "For details on this error, please check the file in the XML editor. It will highlight the respective area. In addition further reports listed below might also contain useful informations on this file, but the file itself could certainly not be parsed for a sophistcated validation."
  echo "</div>" # description

  echo "</div>" # item
  
done < ../report/xmlstarlet-wellformedness.log >> ../report/report.html
echo "</div>" >> ../report/report.html # wellformedness


echo '<div class="jing">' >> ../report/report.html

while read -r line;
do
  FILEPATH=$(echo $line |  cut -f 1 -d \:)
  FILENAME=$(echo $line |  cut -f 1 -d \: | cut -f 7 -d \/)
  LINE=$(echo $line |  cut -f 2 -d \:)
  COLUMN=$(echo $line |  cut -f 3 -d \:)
  TYPE=$(echo $line |  cut -f 4 -d \:)
  DESCRIPTION=$(echo $line |  cut -f 5 -d \: | sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g')
  
  echo "<div class=\"item\" data-type=\"${TYPE}\">"
  
  echo -n '<div class="filename">'
  echo -n "<span title=\"${FILEPATH}\">${FILENAME}</span>"
  echo "</div>" # filename
 
  echo '<div class="description">'
  echo ${DESCRIPTION}
  echo "</div>" # description

  echo -n '<div class="line">'
  echo -n ${LINE}
  echo "</div>" # line

  echo -n '<div class="column">'
  echo -n ${COLUMN}
  echo "</div>" # column

  echo "</div>" # item
  
done < ../report/jing.log >> ../report/report.html
echo "</div>" >> ../report/report.html # jing


echo '<div class="isoschematron" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">' >> ../report/report.html
for FILE in $(grep -l -r "failed" ../report/isosch)
do
    echo "<div class=\"item\">"
    
    echo -n '<div class="filename">'
    echo -n "${FILE}" | sed 's ../report/isosch/  g'
    echo "</div>" # filename

    echo '<div class="description"><ol class="schematron-failed">'
    xmllint --xpath "//*[starts-with(local-name(), 'failed')]"  ${FILE}
    echo "</ol></div>" # description

    echo "</div>" # item
done  >> ../report/report.html

echo "</div>" >> ../report/report.html # isoschematron


echo '<div class="isoschematron-log">' >> ../report/report.html
grep While ../report/schematron.log > /tmp/grep
while read -r line;
do
    echo "<div class=\"item\">"
    
    echo -n '<div class="filename">'
    echo -n ${line} | cut -f3 --delimiter=" " | sed 's \:  g'
    echo "</div>" # filename

    echo '<div class="description">'
    echo ${line}
    echo "</div>" # description

    echo -n '<div class="url">'
    echo -n ${line} | grep --only-matching --extended-regexp "http(s)?\:\/\/.+\s" | sed "s \:\s*$  g"
    echo "</div>" # url

    echo "</div>" # item
done < /tmp/grep >> ../report/report.html

echo "</div>" >> ../report/report.html # isoschematron-log



echo "</div>" >> ../report/report.html # report

xmlstarlet fo ../report/report.html > ../report/report.html.new && mv ../report/report.html.new ../report/report.html

## prepare a list of invalid files
xmlstarlet sel --template --value-of '//div[@class="filename"]' ../report/report.html | sort | uniq > l1.txt

while read -r line; do
  echo ${line} | sed -r 's/\s+//g'
done < l1.txt | sed '/^$/d' > l2.txt

echo "[" > ../schema/report-html/list.json
i=0
t=`wc -l < l2.txt`
while read -r line; do
  i=$((i+1))
  if [[ $i -eq $t ]]
  then
    echo -n "\"$line\"" >> ../schema/report-html/list.json
  else
    echo -n "\"$line\"," >> ../schema/report-html/list.json
  fi
done < l2.txt

echo "]" >> ../schema/report-html/list.json
