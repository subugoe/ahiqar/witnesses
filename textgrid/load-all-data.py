# SPDX-FileCopyrightText: 2023 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: EUPL-1.2

from os import chdir, environ, getcwd, makedirs, path
from tgclients import TextgridCrud, TextgridMetadata
from lxml import etree

def reader(uri, sid, workdir) -> None:
    """
        get metadata and data
    """
    get_meta = tgcrud.read_metadata(textgrid_uri=baseuri, sid=sid)
    datatype = get_meta.object_value.generic.provided.format.split("/")[1].split("+")[0]
    title = get_meta.object_value.generic.provided.title[0]
    get_data = tgcrud.read_data(textgrid_uri=baseuri, sid=sid)

    match datatype:
        case 'tg.edition':
            tree = etree.fromstring(get_data.content)
            namespaces = tree.nsmap
            aggs = tree.findall('.//ore:aggregates', namespaces=namespaces)
            print(f"found edition: ‘{title}’")
            new_path = prepare_aggregation('tg.edition', title, workdir)
            store(get_data.content)

def store(data, metadata, title, workdir):
    return path

def prepare_aggregation(type, title, workdir) -> str:
    """
        prepares directories and files
    """
    new_path = workdir + '/' + title
    if not path.exists(title):
        makedirs(new_path) # URL Encoding here to deal with slash in title?
    return new_path

sid='1rmw0GRsFnnrZOSbjBqmpETnSLAkUFUDO0Kb9Gc9ESP3s411H4mTq1687768095190012'
baseuri='textgrid:3r132'
tgcrud=TextgridCrud(nonpublic=True)

if path.exists(getcwd() + '/.git'):
    currentDir = getcwd() + '/data'
else:
    currentDir = getcwd()


reader(baseuri, sid, currentDir)

print('done.')